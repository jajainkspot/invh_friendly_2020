function animate(){

	//contstruct the animation timeline
	var timeline = gsap.timeline({repeat:0,delay:.25})
	.set(".adContent", {visibility:"visible"})

	/***** animate in frame 1 ****/
	.from("#txt1", 1, {autoAlpha:0},"+=.25")

	/***** animate in frame 2 ****/
	.to("#txt1", 1.5, {autoAlpha:0, ease:Power2.easeOut}, "+=2")
	.to("#image1", 1.5, {autoAlpha:0, ease:Power2.easeOut}, "-=1.5")
	
	.from("#image2", 2, {autoAlpha:0, ease:Power2.easeOut}, "-=0.6")
	.from("#txt2", 1.5, {autoAlpha:0, ease:Power2.easeOut}, "-=1.7")
	
	/***** animate in frame 3 ****/
	.to("#txt2", 1.5, {autoAlpha:0, ease:Power2.easeOut}, "+=2")
	.to("#image2", 1.5, {autoAlpha:0, ease:Power2.easeOut}, "-=1.5")
	
	.from("#image3", 2, {autoAlpha:0, ease:Power2.easeOut}, "-=0.6")
	.from("#txt3", 1.5, {autoAlpha:0, ease:Power2.easeOut}, "-=1.7")

	/***** animate out frame 4 ****/
	.to("#txt3", 1.5, {autoAlpha:0, ease:Power2.easeOut}, "+=2")
	.to("#image3", 1.5, {autoAlpha:0, ease:Power2.easeOut}, "-=1.5")
	
	.from("#house", 2, {autoAlpha:0, ease:Power2.easeOut}, "-=0.6")
	.from("#txt4", 1.5, {autoAlpha:0, ease:Power2.easeOut}, "-=1.7")

	/***** animate in CTA ****/
	.from(".cta", 1, {scale:0, autoAlpha:0, transformOrigin: "150px 430px", ease:Bounce.easeOut, onComplete:setRollovers},"+=0.2");

	//Initiatie RollOvers
	function setRollovers(){
	  document.getElementById("ad-tbb").addEventListener("mouseover", adTBBrollOver);
	  document.getElementById("ad-tbb").addEventListener("mouseout", adTBBrollOut);
	}

}
